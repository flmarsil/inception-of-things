#!/bin/bash

GREEN='\033[0;32m'
YELLOW='\033[0;33m'
NC='\033[0m'

# testing docker installation
docker > /dev/null 2>&1
if [ $? -ne 0 ]
then

	echo "${YELLOW}DOCKER INSTALLATION ...${NC}"

	# install docker
	sudo apt-get install -y \
	    apt-transport-https \
	    ca-certificates \
	    curl \
	    gnupg \
	    lsb-release

	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

	echo \
	  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
	  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

	sudo apt-get update -y
	sudo apt-get install -y docker-ce docker-ce-cli containerd.io
	
	# manage Docker as a non-root user
	# create the docker group
	sudo groupadd docker || true 
	# Add your user to the docker group.
	sudo usermod -aG docker $USER

else
	echo "${GREEN}docker is already installed.${NC}"
fi

# https://docs.docker.com/engine/install/ubuntu/
# https://docs.docker.com/engine/install/linux-postinstall/