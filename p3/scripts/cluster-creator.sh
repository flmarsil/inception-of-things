#!/bin/ksh

# create k3d cluster
newgrp docker << EONG
k3d cluster create ${CLUSTERNAME} -p 8888:8888@loadbalancer

# namespace containing argocd
kubectl create namespace argocd

# namespace containing app
kubectl create namespace dev

NEWPASSWORD=$NEWPASSWORD sh ./argocd-installer.sh

EONG


# k3d cluster create inception-of-things --api-port 6443 -p 8080:80@loadbalancer --agents 2