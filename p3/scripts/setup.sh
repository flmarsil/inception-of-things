#!/bin/bash

NEWPASSWORD_LENGTH=$(echo ${#NEWPASSWORD} 2>/dev/null)
while [ -z $NEWPASSWORD_LENGTH -o $NEWPASSWORD_LENGTH -le 7 ];
do
    read -sp 'Define a new password for argoCD (more than 8 characters) :' NEWPASSWORD
    echo
    NEWPASSWORD_LENGTH=$(echo ${#NEWPASSWORD} 2>/dev/null)
    if [ $NEWPASSWORD_LENGTH -le 7 ]
    then
        echo 'password too short'
    fi
done

# update 
# sudo apt-get update -y
sudo apt-get install -y ksh
#sudo apt-get upgrade -y

# install requirements
sh ./docker-installer.sh
sh ./kubectl-installer.sh
sh ./k3d-installer.sh

rm kubectl

# create k3d cluster
export CLUSTERNAME=inception-of-things
export NEWPASSWORD=$NEWPASSWORD
sh ./cluster-creator.sh

# change namespace
# kubectl config set-context --current --namespace=dev

# verify change namespace is OK
# kubectl config view --minify | grep namespace:

# sync argocd from cli
# argocd app sync inceptionapp