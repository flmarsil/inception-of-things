# inception_2

## Hosts

You have to add app1.com / app2.com / app3.com to your /etc/hosts file :

``` bash
# inception-of-things project
192.168.42.110 app1.com app2.com app3.com
```

## Ressources

* https://www.youtube.com/watch?v=NChhdOZV4sY&t=88s&ab_channel=Cookieconnect%C3%A9
* https://k33g.gitlab.io/articles/2020-02-23-K3S-04-BETTER-DEPLOY.html
* https://www.youtube.com/watch?v=QcC-5fRhsM8&ab_channel=carpie.net
* https://medium.com/google-cloud/deploying-service-or-ingress-on-gke-59a49b134e3b
* https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nginx-ingress-on-digitalocean-kubernetes-using-helm
