# inception_1

## Up

### Ssh

You have to generate ssh private and public key (id_rsa/id_rsa.pub) with _ssh-keygen_ and to put them in a __.ssh__ directorie at root.

```
ssh-keygen -t rsa -b 4096
```

## Ressources

* install kubectl: https://kubernetes.io/fr/docs/tasks/tools/install-kubectl/
* install vagrant: https://linuxize.com/post/how-to-install-vagrant-on-ubuntu-20-04/
* vagrantFile doc: https://www.vagrantup.com/docs/vagrantfile/machine_settings
* vagrant multi-machine: https://www.vagrantup.com/docs/multi-machine
* vagrant cpu/ram: https://www.vagrantup.com/docs/providers/virtualbox/configuration
* vagrant private_network: https://www.vagrantup.com/docs/networking/private_network
* install k3s: https://www.grottedubarbu.fr/installation-de-k3s-sur-un-vps-ovh/

## Command sheet / Alias

```
# ===================================================== Kubectl
alias kk="kubectl"

# ===================================================== Vagrant
alias vv="vagrant"
alias vvu="vagrant up"                              # up a vagrant machine
alias vvs="vagrant ssh"                             # ssh to a vagrant machine
alias vvh="vagrant halt"                            # stop a vagrant machine
alias vvd="vagrant destroy -f"                      # destroy a vagrant machine
alias vvhd="vagrant halt && vagrant destroy -f"     # stop and destroy a vagrant machine
alias vvg="vagrant global-status"                   # show vms status
alias vvgs="vagrant global-status"                  # show vms status
alias vvls="vagrant global-status"                  # show vms status
alias vvps="vagrant global-status"                  # show vms status
```