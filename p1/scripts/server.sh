#!/bin/sh

# ressources:
#   * https://www.grottedubarbu.fr/installation-de-k3s-sur-un-vps-ovh/

# install k3s /kubectl
# [--write-kubeconfig-mode 644] arg resolves the "open /etc/rancher/k3s/k3s.yaml: permission denied" error
# ^ https://github.com/k3s-io/k3s/issues/389 ^

# other ressources:
# https://www.it-swarm-fr.com/fr/mysql/comment-se-connecter-au-serveur-mysql-dans-virtualbox-vagrant/1066932566/
# https://stackoverflow.com/questions/66449289/is-there-any-way-to-bind-k3s-flannel-to-another-interface
# https://github.com/biggers/vagrant-kubernetes-by-k3s/blob/master/Vagrantfile
# https://tferdinand.net/creer-un-cluster-kubernetes-local-avec-vagrant/

echo "hello world! I'm the Server"

# config environment
hostname=$(hostname)

# install ifconfig
yum install net-tools -y

# configure ssh
mkdir /root/.ssh/
mv /tmp/id_rsa*  /root/.ssh/
chmod 400 /root/.ssh/id_rsa*
chown root:root  /root/.ssh/id_rsa*

cat /root/.ssh/id_rsa.pub >> /root/.ssh/authorized_keys
chmod 400 /root/.ssh/authorized_keys
chown root:root /root/.ssh/authorized_keys

# configure hosts
echo "127.0.1.1 $(hostname)" >> /etc/hosts

# get the current ip
current_ip=$(/sbin/ip -o -4 addr list eth1 | awk '{print $4}' | cut -d/ -f1)

# install k3s/kubectl and
# up a master server
curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC="server --cluster-init --tls-san $(hostname) --bind-address=${current_ip} --advertise-address=${current_ip} --node-ip=${current_ip} --no-deploy=traefik --write-kubeconfig-mode 644" sh -

# kubectl taint
echo "wait for kubectl tain..."
false
while [ $? -ne 0 ]; do
    sleep 2
    # kubectl is not in $PATH at this step
    /usr/local/bin/kubectl taint --overwrite node ${hostname,,} node-role.kubernetes.io/master=true:NoSchedule 2> /dev/null
done
